This is some security associate issue I encounter in process of my xpdf fuzz test

## 1 Denied of service

in `gmallocn` function , Xpdf service will down when meeting condition `nObjs >= INT_MAX / objSize` 

`INT_MAX / objSize` value is `0x7ffffff` （when objSize is `0x10`）

```cpp
void *gmallocn(int nObjs, int objSize) GMEM_EXCEP {
  int n;

  if (nObjs == 0) {
    return NULL;
  }
  n = nObjs * objSize;
  if (objSize <= 0 || nObjs < 0 || nObjs >= INT_MAX / objSize) {
    gMemError("Bogus memory allocation size");
  }
  return gmalloc(n);
}
```



backtrace the caller in `XRef::constructObjectEntry` member function

num's initial value is `0` and max value designed to limit about  `100000000` ， namely `0x5f5e100` ,`0x5f5e100` < `0x7ffffff`

but once num got close to the limit value and executes more the loop again

the num's value will be up to `100000000 × 10`, namely `0x3b9aca00`,   `0x3b9aca00` > `0x7ffffff` will trigger abort

```cpp
char *XRef::constructObjectEntry(char *p, GFileOffset pos, int *objNum) {
  // we look for non-end-of-line space characters here, to deal with
  // situations like:
  //    nnn          <-- garbage digits on a line
  //    nnn nnn obj  <-- actual object
  // and we also ignore '\0' (because it's used to terminate the
  // buffer in this damage-scanning code)
  int num = 0;
  do {
    num = (num * 10) + (*p - '0');
    ++p;
  } while (*p >= '0' && *p <= '9' && num < 100000000);
  if (*p != '\t' && *p != '\x0c' && *p != ' ') {
    return p;
  }
  
    (···)

  if (constructXRefEntry(num, gen, pos - start, xrefEntryUncompressed)) {
    *objNum = num;
  }

  return p;
}
```

`constructObjectEntry` pass `num` to `constructXRefEntry`

`constructXRefEntry` align `num` to `0x100` and pass the new variable `newSize` to `greallocn`

cause the error and then abort

```cpp
GBool XRef::constructXRefEntry(int num, int gen, GFileOffset pos,
			       XRefEntryType type) {
    
    int newSize = (num + 1 + 255) & ~255;// align to 0x100
   (···)
    
    entries = (XRefEntry *)greallocn(entries, newSize, sizeof(XRefEntry));


  (···)

  return gTrue;
}
```

### backtrace

function call chain backtrace in gdb is as follows

```c
__GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:50
__GI_abort () at abort.c:79
?? () from /lib/x86_64-linux-gnu/libstdc++.so.6
?? () from /lib/x86_64-linux-gnu/libstdc++.so.6
std::terminate() () from /lib/x86_64-linux-gnu/libstdc++.so.6
__cxa_throw () from /lib/x86_64-linux-gnu/libstdc++.so.6
gMemError (msg=msg@entry=0x555555981415 "Bogus memory allocation size") at /home/squ/fuzzproj/xpdf-4.04/goo/gmem.h:19
greallocn (p=0x555555a446d0, nObjs=<optimized out>, objSize=<optimized out>) at /home/squ/fuzzproj/xpdf-4.04/goo/gmem.cc:302
XRef::constructXRefEntry (this=this@entry=0x555555a43e40, num=num@entry=900000147, gen=0, pos=2456, type=type@entry=xrefEntryUncompressed) at /home/squ/fuzzproj/xpdf-4.04/xpdf/XRef.cc:1111
XRef::constructObjectEntry (this=this@entry=0x555555a43e40, p=0x7fffffffd8f5 "obj\r\n<<\r\n/Type /Font\r\n/Subtype /Type1\r\n/Name /F1\r\n/BaseFont /Helvetica\r\n/Encoding /WinAnsiEncoding\r\n>>\r\nendobj\r\n\r\n10 0 obj\r\n<<\r\n/Creator (Rave \\(http://www.nevrona.com/rave\\))\r\n/Producer (Nevrona Desi"..., p@entry=0x7fffffffd8e8 "900000147  0 obj\r\n<<\r\n/Type /Font\r\n/Subtype /Type1\r\n/Name /F1\r\n/BaseFont /Helvetica\r\n/Encoding /WinAnsiEncoding\r\n>>\r\nendobj\r\n\r\n10 0 obj\r\n<<\r\n/Creator (Rave \\(http://www.nevrona.com/rave\\))\r\n/Producer "..., pos=pos@entry=2456, objNum=objNum@entry=0x7fffffffcf2c) at /home/squ/fuzzproj/xpdf-4.04/xpdf/XRef.cc:1062
XRef::constructXRef (this=this@entry=0x555555a43e40) at /home/squ/fuzzproj/xpdf-4.04/xpdf/XRef.cc:913
XRef::XRef (this=0x555555a43e40, strA=0x555555a41e70, repair=1) at /home/squ/fuzzproj/xpdf-4.04/xpdf/XRef.cc:336
PDFDoc::setup2 (this=0x555555a41e10, ownerPassword=0x0, userPassword=0x0, repairXRef=1) at /home/squ/fuzzproj/xpdf-4.04/xpdf/PDFDoc.cc:300
PDFDoc::setup (this=0x555555a41e10, ownerPassword=0x0,
 userPassword=0x0) at /home/squ/fuzzproj/xpdf-4.04/xpdf/PDFDoc.cc:276
in PDFDoc::PDFDoc (this=0x555555a41e10, fileNameA=0x7fffffffe55d "./id:000000,sig:06,src:000000,time:1816479,execs:269386,op:havoc,rep:8", ownerPassword=0x0, userPassword=0x0, coreA=<optimized out>) at /home/squ/fuzzproj/xpdf-4.04/xpdf/PDFDoc.cc:218
main (argc=<optimized out>, argc@entry=3, argv=<optimized out>, argv@entry=0x7fffffffe258) at /home/squ/fuzzproj/xpdf-4.04/xpdf/pdftotext.cc:232
__libc_start_main (main=0x5555555eded0 <main(int, char**)>, argc=3, argv=0x7fffffffe258, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffffffe248) at ../csu/libc-start.c:308
in _start ()
```

simplified version

```c
gMemError
greallocn
XRef::constructXRefEntry
XRef::constructObjectEntry 
XRef::constructXRef 
XRef::XRef 
PDFDoc::setup2
PDFDoc::setup
PDFDoc::PDFDoc
mains
```

### fix advice

lower the max value of num in `while loop` limit condition

```c
  do {
    num = (num * 10) + (*p - '0');
    ++p;
  } while (*p >= '0' && *p <= '9' && num < 100000000);
```



## 2 infinite recursion

This vulnerability I can't analyze very clearly

infinite recursion in `Catalog::countPageTree` function 

```cpp
int Catalog::countPageTree(Object *pagesObj) {

  (···)
    
  if (pagesObj->dictLookup("Kids", &kids)->isArray()) {
    n = 0;
    for (i = 0; i < kids.arrayGetLength(); ++i) {
      kids.arrayGet(i, &kid);
      n2 = countPageTree(&kid);// <-<<-<-<<- vuln
   
  (···)   
    
}
```

condition `pagesObj->dictLookup("Kids", &kids)->isArray()` will be met permanently

because

```c
Object *Dict::lookup(const char *key, Object *obj, int recursion) {
  DictEntry *e;

  return (e = find(key)) ? e->val.fetch(xref, obj, recursion)// <<- into there
                         : obj->initNull();
}
```

e will find successfully ,then call `e->val.fetch(xref, obj, recursion)` to get object

and `type == objRef && xref` condition not meet

call `copy(obj)` to return a obj whoes type is `objArray`

```c
Object *Object::fetch(XRef *xref, Object *obj, int recursion) {
  return (type == objRef && xref) ? // objRef != objArray
         xref->fetch(ref.num, ref.gen, obj, recursion) : copy(obj);// return objArray
}
```

finally infinite call `Catalog::countPageTree` to consume stack space (PoC will be provided in attachments)

```c
Program received signal SIGSEGV, Segmentation fault.
(···)
pwndbg> x/gx $rsp
0x7fffff7fefc8: Cannot access memory at address 0x7fffff7fefc8
```

```c
➜  crashes $XPDF id:000003,sig:11,src:000105,time:7086222,execs:1098970,op:arith8,pos:
192,val:-1 $XPATH/output
[1]    2388367 segmentation fault  $XPDF  $XPATH/output
```



## 3 epilog

verify with gdb

```shell
export XPDF=/path/to/your/pdftotext
export XPATH=/path/to/your/xpdf-4.04/
gdb --args $XPDF ./PoC $XPATH/output
```

![image-20220723192124367](./report.assets/image-20220723192124367.png)

